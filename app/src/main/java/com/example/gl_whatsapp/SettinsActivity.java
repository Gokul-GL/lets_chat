package com.example.gl_whatsapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.storage.StorageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettinsActivity extends AppCompatActivity
{
    private Button UpdateAccountSetting,postsbutton;
    private EditText username,userstatus,MobileNumber;
    private CircleImageView userProfileImage;
    private String currentUserId;
    private FirebaseAuth mAthu;
    private DatabaseReference RootRef;
    private static final int GalleryPick=1;
    private StorageReference UserProfileImageRef;
    private ProgressDialog lodingBar;
    private Toolbar mToolbar;
    private Uri resultUri;
    private String downloadUri;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settins);
        InitializeFields();
        username.setVisibility(View.INVISIBLE);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Account Settings");

        lodingBar=new ProgressDialog(this);
        mAthu=FirebaseAuth.getInstance();
        currentUserId=mAthu.getCurrentUser().getUid();
        RootRef= FirebaseDatabase.getInstance().getReference();
        UserProfileImageRef= FirebaseStorage.getInstance().getReference().child("Profile Image");

        UpdateAccountSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateSettings();
            }
        });
        RetriveUserInformation();

        userProfileImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent galleryintent=new Intent();
                galleryintent.setAction(Intent.ACTION_GET_CONTENT);
                galleryintent.setType("image/*");
                startActivityForResult(galleryintent, GalleryPick);
            }
        });
    }


    private void InitializeFields()
    {
        UpdateAccountSetting=findViewById(R.id.button);
        username=findViewById(R.id.editText2);
        userstatus=findViewById(R.id.editText3);
        userProfileImage=findViewById(R.id.profile_image);
        mToolbar=findViewById(R.id.find_settings_toolbar);
       MobileNumber=findViewById(R.id.mobile_number);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==GalleryPick &&  resultCode==RESULT_OK && data!=null)
        {
            Uri ImageUri=data.getData();
            CropImage.activity(ImageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this);

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK)
            {
                resultUri = result.getUri();
                userProfileImage.setImageURI(resultUri);
            }
           else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
            {
                Exception error = result.getError();
                Toast.makeText(this, ""+error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void UpdateSettings()
    {




        String setUserName=username.getText().toString();
        String setStatus=userstatus.getText().toString();
        String setMobileNumber=MobileNumber.getText().toString();



        if (TextUtils.isEmpty(setUserName))
        {
            Toast.makeText(this, "Please Enter Your UserName...", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(setStatus))
        {
            Toast.makeText(this, "Please Enter Your Status...", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(setMobileNumber))
        {
            Toast.makeText(this, "Please Enter Your Mobile Number", Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(setUserName)&&TextUtils.isEmpty(setStatus)&&TextUtils.isEmpty(setMobileNumber))
        {
            Toast.makeText(this, "Please Enter Your Name & Status...", Toast.LENGTH_SHORT).show();
        }
        else
        {
            lodingBar.setTitle("Updating Profile Data");
            lodingBar.setMessage("Please Wait , your profile data is updating ...");
            lodingBar.setCanceledOnTouchOutside(false);
            lodingBar.show();
            SaveInformation(setUserName, setStatus,setMobileNumber);
        }
    }

    private void SaveInformation(final String setUserName, final String setStatus,final String setMobileNumber)
    {

        if (resultUri != null)
        {
            StorageReference filepath=UserProfileImageRef.child(currentUserId +".jpg");

            filepath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>()
            {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task)
                {
                    if (task.isSuccessful())
                    {
                         downloadUri=Objects.requireNonNull(task.getResult().getDownloadUrl()).toString();
                        updateInformation(setUserName,setStatus,setMobileNumber);
                    }
                    else
                    {
                        String Error=task.getException().toString();
                        Toast.makeText(SettinsActivity.this, ""+Error, Toast.LENGTH_SHORT).show();
                        lodingBar.dismiss();
                    }
                }
            });
        }

    }
    private void updateInformation( String setUserName, String setStatus,String setMobileNumber)
    {
        HashMap<String,Object> profilemap=new HashMap<>();

        profilemap.put("uid",currentUserId);
        profilemap.put("name",setUserName);
        profilemap.put("status",setStatus);
        profilemap.put("Mobile_Number",setMobileNumber);
        profilemap.put("profileimage",downloadUri);
        RootRef.child("Users").child(currentUserId).updateChildren(profilemap)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        if (task.isSuccessful())
                        {
                            SendUserToMainActivity();
                            Toast.makeText(SettinsActivity.this, "Profile Updated Successfulyy....", Toast.LENGTH_SHORT).show();
                            lodingBar.dismiss();
                        }
                        else
                        {
                            String Error=task.getException().toString();
                            Toast.makeText(SettinsActivity.this, ""+Error, Toast.LENGTH_SHORT).show();
                            lodingBar.dismiss();
                        }
                    }
                });
    }
    private void RetriveUserInformation()
    {
        RootRef.child("Users").child(currentUserId).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if ((dataSnapshot.exists()) && (dataSnapshot.hasChild("name")) && (dataSnapshot.hasChild("status")) && (dataSnapshot.hasChild("profileimage"))&&(dataSnapshot.hasChild("Mobile_Number")))
                {
                    String retriveUsername=dataSnapshot.child("name").getValue().toString();
                    String retriveUserstatus=dataSnapshot.child("status").getValue().toString();
                   String retriveProfileImage=dataSnapshot.child("profileimage").getValue().toString();
                    String retriveMobileNumber=dataSnapshot.child("Mobile_Number").getValue().toString();
                    username.setText(retriveUsername);
                    userstatus.setText(retriveUserstatus);
                    MobileNumber.setText(retriveMobileNumber);

                   Picasso.get().load(retriveProfileImage).into(userProfileImage);

                }
                else if ((dataSnapshot.exists())&&(dataSnapshot.hasChild("name")))
                {
                    String retriveUsername=dataSnapshot.child("name").getValue().toString();
                    String retriveUserstatus=dataSnapshot.child("status").getValue().toString();
                    username.setText(retriveUsername);
                    userstatus.setText(retriveUserstatus);
                }
                else
            {
                username.setVisibility(View.VISIBLE);
                Toast.makeText(SettinsActivity.this, "Please set & update profile information....", Toast.LENGTH_SHORT).show();
            }

        }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    private void SendUserToMainActivity()
    {

        Intent mainintent=new Intent(SettinsActivity.this,MainActivity.class);
        mainintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainintent);
        finish();


    }

}
