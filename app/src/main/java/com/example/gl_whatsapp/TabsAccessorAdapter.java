package com.example.gl_whatsapp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

public class TabsAccessorAdapter extends FragmentPagerAdapter
{
    public TabsAccessorAdapter(FragmentManager fm)
    {
        super(fm);
    }
    public static StatusFragment statusFragment;
    public static ChatsFragment chatsFragment;
    @Override
    public Fragment getItem(int i)
    {
        switch (i)
        {
            case 0:
                chatsFragment=new ChatsFragment();
                return chatsFragment;
            case 1:
                statusFragment =new StatusFragment();
                return statusFragment;

            case 2:
                ContactsFragment contactsFragment=new ContactsFragment();
                return contactsFragment;
            default:
                return null;
        }
    }
    @Override
    public int getCount()
    {
        return 3;
    }
    @Nullable
    @Override
    public CharSequence getPageTitle(int position)
    {
        switch (position)
        {
            case 0:
                return "Chats";
            case 1:
                return "Status";
            case 2:
                return "Contacts";
            default:
                return null;
        }
    }
    private Fragment mCurrentFragment;

    public Fragment getCurrentFragment()
    {
        return mCurrentFragment;
    }
    @Override
    public void setPrimaryItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        if (getCurrentFragment() != object) {
            mCurrentFragment = ((Fragment) object);
        }
        super.setPrimaryItem(container, position, object);
    }
}
