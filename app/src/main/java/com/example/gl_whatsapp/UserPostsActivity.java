package com.example.gl_whatsapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserPostsActivity extends AppCompatActivity
{
    private RecyclerView postList;
    private DatabaseReference PostsRef,LikesRef,PostRef;
    private int i=0;
    private String State="play";
    private Boolean LikeChecker=false;
    private FirebaseAuth mAuth;
    private String currentUserID;
    String currentUserId;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_posts);

        postList=findViewById(R.id.userpostList);
       postList.setLayoutManager(new LinearLayoutManager(this));
        mAuth=FirebaseAuth.getInstance();
        currentUserID=mAuth.getCurrentUser().getUid();
        LikesRef = FirebaseDatabase.getInstance().getReference().child("Likes");
        currentUserId=FirebaseAuth.getInstance().getCurrentUser().getUid();
        PostsRef=FirebaseDatabase.getInstance().getReference().child("Posts");
       // PostRef=FirebaseDatabase.getInstance().getReference().child("Posts").child(currentUserID);

        LikesRef=FirebaseDatabase.getInstance().getReference().child("Likes");

    }
    @Override
    public void onStart()
    {
        super.onStart();
        FirebaseRecyclerOptions<Posts> options=new FirebaseRecyclerOptions.Builder<Posts>()
                .setQuery(PostsRef.orderByChild("counter"),Posts.class)
                .build();
        FirebaseRecyclerAdapter<Posts,PostsViewHolder> adapter=new FirebaseRecyclerAdapter<Posts, PostsViewHolder>(options)
        {
            @Override
            protected void onBindViewHolder(@NonNull final PostsViewHolder viewHolder, int position, @NonNull final Posts model)
            {
                final String PostKey = getRef(position).getKey();
                final ProgressDialog pd = new ProgressDialog(UserPostsActivity.this);
                PostsRef.child("Posts").child(currentUserId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        if (dataSnapshot.exists()&&(dataSnapshot.hasChild("name")))
                        {
                            String rname=dataSnapshot.child("Posts").child("name").getValue().toString();


                            pd.setMessage("Loading Video");
                            pd.show();
                            viewHolder.username.setText(rname);
                            // viewHolder.Time.setText(model.getTime());
                            viewHolder.Date.setText(model.getDate() + " ," + model.getTime());
                            viewHolder.discription.setText(model.getDescription());
                            // viewHolder.setProfileimage(getActivity(), model.getProfileimage());
                            viewHolder.setPostvideo(UserPostsActivity.this, model.getPostvideo());
                            Picasso.get().load(model.getProfileimage()).placeholder(R.drawable.profile_image).into(viewHolder.image);
                        }else
                        {
                          //  viewHolder.setPostvideo("No Videos Available");
                            Toast.makeText(UserPostsActivity.this, "No Videos are  Available in your Post list", Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


//                        String video=model.getPostvideo();
//                               viewHolder.videoView.setVideoURI(Uri.parse(video));

                viewHolder.LikeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        LikeChecker = true;

                        LikesRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                if (LikeChecker.equals(true)) {

                                    if (dataSnapshot.child(PostKey).hasChild(currentUserID)) {

                                        LikesRef.child(PostKey).child(currentUserID).removeValue();
                                        LikeChecker = false;


                                    } else {

                                        LikesRef.child(PostKey).child(currentUserID).setValue(true);
                                        LikeChecker = false;


                                    }

                                }


                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }
                });


                viewHolder.CommentButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {

                        startActivity(new Intent(UserPostsActivity.this, CommentsActivity.class).putExtra("PostKey", PostKey));

                    }
                });



                    viewHolder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {

                            pd.dismiss();
                            State = "play";
                            viewHolder.videoView.start();
                            viewHolder.videoView.requestFocus();


                        }
                    });


                LikesRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.child(PostKey).hasChild(currentUserId))
                        {

                            viewHolder.countLikes=(int)  dataSnapshot.child(PostKey).getChildrenCount();

                            viewHolder.LikeButton.setImageResource(R.drawable.likes);

                            viewHolder.LikesNo.setText(Integer.toString( viewHolder.countLikes)+(" "));

                        }else
                        {

                            viewHolder.countLikes=(int)  dataSnapshot.child(PostKey).getChildrenCount();

                            viewHolder.LikeButton.setImageResource(R.drawable.like);

                            viewHolder.LikesNo.setText(Integer.toString( viewHolder.countLikes)+(" "));


                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                viewHolder.videoView.setOnTouchListener(new View.OnTouchListener()
                {
                    @Override
                    public boolean onTouch(View v, MotionEvent event)
                    {

                        if (State.equals("pause"))
                        {
                            viewHolder.videoView.start();
                            viewHolder.videoView.requestFocus();
                            State = "play";
                        } else if (State.equals("play"))
                        {
                            viewHolder.videoView.pause();
                            viewHolder.videoView.requestFocus();
                            State = "pause";
                        }

                        return true;
                    }
                });

            }

            @NonNull
            @Override
            public PostsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
            {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_users_post_layout, parent, false);
                PostsViewHolder viewHolder = new PostsViewHolder(view);
                return viewHolder;

            }
        };
        postList.setAdapter(adapter);
        adapter.startListening();



    }

    public static class PostsViewHolder extends RecyclerView.ViewHolder
    {
        View mView;

        ImageView LikeButton,CommentButton;
        TextView LikesNo;
        int countLikes;
        DatabaseReference LikesRef;
        TextView username,Date,Time,discription;
        VideoView videoView;
        CircleImageView image;

        public PostsViewHolder(View itemView)
        {
            super(itemView);
            mView = itemView;

            username=mView.findViewById(R.id.userName);
            videoView=mView.findViewById(R.id.userVideo);

            LikeButton=mView.findViewById(R.id.likeButton);
            CommentButton=mView.findViewById(R.id.commentButton);
            Date=mView.findViewById(R.id.post_date);
            //Time=mView.findViewById(R.id.post_time);
            discription=mView.findViewById(R.id.descrip);
            LikesNo=mView.findViewById(R.id.display_no_of_likes);
            image=mView.findViewById(R.id.userImage);

        }
        public void setPostvideo(Context ctx, String postvideo)
        {

            if (postvideo!=null)
            {
                videoView.setVideoURI(Uri.parse(postvideo));

            }

        }


    }
}
