package com.example.gl_whatsapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.ShadowDrawableWrapper;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gl_whatsapp.notification.Token;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import static com.google.firebase.iid.FirebaseInstanceId.*;

public class MainActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private ViewPager myViewPager;
    private TabLayout myTabLayout;
    private TabsAccessorAdapter myTabsAccessorAdapter;
    private FirebaseUser currentUser;
    private FirebaseAuth mAthu;
    private DatabaseReference RootRef;
    private String CurrentUserId,mUser;
   private String deviceToken;
    public static MainActivity mainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivity = this;
        mAthu=FirebaseAuth.getInstance();
        RootRef= FirebaseDatabase.getInstance().getReference();
        mToolbar=findViewById(R.id.main_page_toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("Let's Chat");
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.black));
        myViewPager=findViewById(R.id.main_tabs_pager);
        myTabsAccessorAdapter=new TabsAccessorAdapter(getSupportFragmentManager());
        myViewPager.setAdapter(myTabsAccessorAdapter);
        myTabLayout=findViewById(R.id.mail_tabs);
        myTabLayout.setupWithViewPager(myViewPager);

       deviceToken= FirebaseInstanceId.getInstance().getToken();
        updateToken(deviceToken);

        myViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int i, float v, int i1)
            {

            }

            @Override
            public void onPageSelected(int i) {
                if (i==1){

                    TabsAccessorAdapter.statusFragment.setFragment(StatusFragment.postFragment);
                }else {
                    TabsAccessorAdapter.statusFragment.setFragment(StatusFragment.addFragment);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    @Override
    protected void onStart()
    {
            super.onStart();
            currentUser=mAthu.getCurrentUser();
            updateUserStatus("online");
            VerifyUserExistance();
            CurrentUserId=mAthu.getCurrentUser().getUid();
            SharedPreferences sp=getSharedPreferences("SP_USER",MODE_PRIVATE);
            SharedPreferences.Editor editor=sp.edit();
            editor.putString("Current_UID",CurrentUserId);
            editor.apply();
    }
    @Override
    protected void onStop()
    {
        super.onStop();
        currentUser=mAthu.getCurrentUser();
        if (currentUser != null)
        {
            updateUserStatus("offline");
        }
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        currentUser=mAthu.getCurrentUser();
        if (currentUser != null)
        {
            updateUserStatus("offline");
        }
    }
    private void VerifyUserExistance()
    {
        String currentUserID=mAthu.getCurrentUser().getUid();
        RootRef.child("Users").child(currentUserID).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if ((dataSnapshot.child("name").exists()))
                {
                   // Toast.makeText(MainActivity.this, "Welcome", Toast.LENGTH_SHORT).show();
                }
                else
                {

                    SendUserToSettingsActivity();
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    private void SendUserToLoginActivity()
    {
        Intent loginintent=new Intent(MainActivity.this,LoginActivity.class);
        loginintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginintent);
        finish();

    }
    private void SendUserToSettingsActivity()
    {
        Intent settingsintent=new Intent(MainActivity.this,SettinsActivity.class);
        startActivity(settingsintent);


    }
    private void SendUserToFindFriendsActivity()
    {
        Intent findfriendsintent=new Intent(MainActivity.this,FindFriendsActivity.class);
        startActivity(findfriendsintent);


    }
    private void SendUserToRequestsActivity()
    {
        Intent requestintent=new Intent(MainActivity.this, RequestsActivity.class);
        startActivity(requestintent);
    }

    private void updateUserStatus(String state)
    {
        String SaveCurrentTime,SaveCurrentDate;
        Calendar calForData= Calendar.getInstance();
        SimpleDateFormat currentDateFormat=new SimpleDateFormat("MMM dd,yyyy");
        SaveCurrentDate=currentDateFormat.format(calForData.getTime());

        Calendar calForTime= Calendar.getInstance();
        SimpleDateFormat currentTimeFormat=new SimpleDateFormat("hh:mm a");
        SaveCurrentTime=currentTimeFormat.format(calForTime.getTime());


        HashMap<String,Object> onlineStateMap=new HashMap<>();
        onlineStateMap.put("time",SaveCurrentTime);
        onlineStateMap.put("date",SaveCurrentDate);
        onlineStateMap.put("state",state);
        CurrentUserId=mAthu.getCurrentUser().getUid();
        RootRef.child("Users").child(CurrentUserId).child("userState")
                .updateChildren(onlineStateMap);


    }


    public void updateToken(String token)
    {
        CurrentUserId=mAthu.getCurrentUser().getUid();
        DatabaseReference ref=FirebaseDatabase.getInstance().getReference("Tokens");
        Token mtoken=new Token(token);
        ref.child(CurrentUserId).setValue(mtoken);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.options_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        super.onOptionsItemSelected(item);
        if (item.getItemId()==R.id.main_logout_option)
        {
            updateUserStatus("offline");
            mAthu.signOut();
            SendUserToLoginActivity();
        }
        if (item.getItemId()==R.id.main_requsts_option)
        {
            SendUserToRequestsActivity();
        }

        if (item.getItemId()==R.id.main_settings_option)
        {
            SendUserToSettingsActivity();
        }
        if (item.getItemId()==R.id.main_find_friends_option)
        {
            SendUserToFindFriendsActivity();
        }
        return true;
    }



//
//    private void RequestNewGroup()
//    {
//        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
//        builder.setTitle("Enter Your Group Name :");
//        final EditText groupNameFiled=new EditText(MainActivity.this);
//        groupNameFiled.setHint("   E.g : GL_InfoTech");
//        builder.setView(groupNameFiled);
//
//        builder.setPositiveButton("Create", new DialogInterface.OnClickListener()
//        {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i)
//            {
//                String groupname = groupNameFiled.getText().toString();
//
//                if (TextUtils.isEmpty(groupname))
//                {
//                    Toast.makeText(MainActivity.this, "Please Enter Group Name...", Toast.LENGTH_SHORT).show();
//                }
//                else
//                {
//                    CreateNewGroup(groupname);
//                }
//
//            }
//        });
//        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
//        {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i)
//            {
//                dialogInterface.cancel();
//            }
//        });
//        builder.show();
//
//    }
//
//    private void CreateNewGroup(final String groupname)
//    {
//        RootRef.child("Groups").child(groupname).setValue("").addOnCompleteListener(new OnCompleteListener<Void>()
//        {
//            @Override
//            public void onComplete(@NonNull Task<Void> task)
//            {
//               if (task.isSuccessful())
//               {
//                   Toast.makeText(MainActivity.this, groupname+"Group is Created Created Successfulyy...", Toast.LENGTH_SHORT).show();
//               }
//            }
//        });
//    }
}
