package com.example.gl_whatsapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.mbms.MbmsErrors;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.gl_whatsapp.notification.Token;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

public class LoginActivity extends AppCompatActivity
{
    private Button LoginButton;
    private EditText useremail,userpassword;
    private ImageView imageView;
    private TextView NeedNewAcountLink,ForgetPasswordLink;
    private FirebaseAuth mAthu;
    private ProgressDialog LodingBar;
    private FirebaseUser currentUser;
    private DatabaseReference userRef;
    private boolean emailAddressCheker;
    private static final String TAG = "AndroidClarified";
   private SignInButton googleSignInButton;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();



        mAthu=FirebaseAuth.getInstance();
        userRef= FirebaseDatabase.getInstance().getReference().child("Users");
        currentUser=mAthu.getCurrentUser();
        InitalizeFields();
        NeedNewAcountLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendUserToRegisterActivity();
            }
        });

        ForgetPasswordLink.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(LoginActivity.this,ResetPasswordActivity.class);
                startActivity(intent);
            }
        });

        LoginButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                AllowUserToLogin();
            }

            private void AllowUserToLogin()
            {
                String email=useremail.getText().toString();
                String password=userpassword.getText().toString();
                if (TextUtils.isEmpty(email))
                {
                    Toast.makeText(LoginActivity.this, "Please Enter a Email ...", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(password))
                {
                    Toast.makeText(LoginActivity.this, "Please Enter a password ...", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(email)&&TextUtils.isEmpty(password))
                {
                    Toast.makeText(LoginActivity.this, "Please Enter a Email and Password ...", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    LodingBar.setTitle("Sign In");
                    LodingBar.setMessage("Please Wait..");
                    LodingBar.setCanceledOnTouchOutside(true);
                    LodingBar.show();
                    mAthu.signInWithEmailAndPassword(email,password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>()
                            {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task)
                                {
                                  if (task.isSuccessful())
                                  {
                                      final String currentUserId=mAthu.getCurrentUser().getUid();
                                      final String deviceToken= FirebaseInstanceId.getInstance().getToken();
                                      userRef.child(currentUserId).child("device_token")
                                              .setValue(deviceToken)
                                              .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                  @Override
                                                  public void onComplete(@NonNull Task<Void> task)
                                                  {
                                                      if (task.isSuccessful())
                                                      {
//                                                          DatabaseReference ref=FirebaseDatabase.getInstance().getReference("Tokens");
//                                                          Token mtoken=new Token(deviceToken);
//                                                          ref.child(currentUserId).setValue(mtoken);

                                                          VerifyEmailAddres();

                                                          LodingBar.dismiss();

                                                      }

                                                  }
                                              });

                                  }
                                  else
                                  {
                                      String message=task.getException().toString();
                                      Toast.makeText(LoginActivity.this, "Error:"+message, Toast.LENGTH_SHORT).show();
                                      LodingBar.dismiss();

                                  }
                                }
                            });
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        Animation animation=AnimationUtils.loadAnimation(this,R.anim.animation);
        imageView.startAnimation(animation);
        useremail.startAnimation(animation);
        userpassword.startAnimation(animation);
        LoginButton.startAnimation(animation);
    }

    private void VerifyEmailAddres()
    {

        FirebaseUser user= mAthu.getCurrentUser();
        emailAddressCheker=user.isEmailVerified();
        if (emailAddressCheker)
        {
            SendUserToMainActivity();
            Toast.makeText(LoginActivity.this, "Login in Successful..", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "Please Vefify your Account first....", Toast.LENGTH_LONG).show();
            mAthu.signOut();
        }


    }



    private void InitalizeFields()
    {
        LoginButton=findViewById(R.id.button);
       // googleSignInButton=findViewById(R.id.gsi);
        useremail=findViewById(R.id.editText);
        userpassword=findViewById(R.id.editText3);
        NeedNewAcountLink=findViewById(R.id.textView2);
        ForgetPasswordLink=findViewById(R.id.textView);
        imageView=findViewById(R.id.imageView);
        LodingBar=new ProgressDialog(this);

    }

    private void SendUserToMainActivity()
    {

            Intent mainintent=new Intent(LoginActivity.this,MainActivity.class);
            mainintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainintent);
            finish();

    }
    private void SendUserToRegisterActivity()
    {
        Intent registerintent=new Intent(LoginActivity.this,RegisterActivity.class);
        startActivity(registerintent);

    }
//    private void googleSignIn()
//    {
//
//
//    }
}
