package com.example.gl_whatsapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
//import android.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class PostsFragment extends Fragment
{


    private View myView;
    private RecyclerView postList;
    private DatabaseReference PostsRef,LikesRef;
    private int i=0;
    private String State="pause";
    private Boolean LikeChecker=false;
    private FirebaseAuth mAuth;
    private String currentUserID;
    String currentUserId;
    //private ProgressDialog pd;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        myView= inflater.inflate(R.layout.fragment_posts, container, false);
        postList=myView.findViewById(R.id.postList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        postList.setLayoutManager(linearLayoutManager);
        mAuth=FirebaseAuth.getInstance();
        currentUserID=mAuth.getCurrentUser().getUid();
        LikesRef = FirebaseDatabase.getInstance().getReference().child("Likes");
        currentUserId=FirebaseAuth.getInstance().getCurrentUser().getUid();
        PostsRef=FirebaseDatabase.getInstance().getReference().child("Posts");
        LikesRef=FirebaseDatabase.getInstance().getReference().child("Likes");
        onStarts();

        return myView;


    }
//    @SuppressLint("ValidFragment")
//    public PostsFragment(List<Posts> userPosts)
//    {
//        //this.postList=userPosts;
//    }


    public void onStarts()
    {
       //super.onStart();
            FirebaseRecyclerOptions<Posts> options=new FirebaseRecyclerOptions.Builder<Posts>()
                        .setQuery(PostsRef.orderByChild("counter"),Posts.class)
                        .build();
                FirebaseRecyclerAdapter<Posts,PostsViewHolder> adapter=new FirebaseRecyclerAdapter<Posts, PostsViewHolder>(options)
                {
                    @Override
                    protected void onBindViewHolder(@NonNull final PostsViewHolder viewHolder, int position, @NonNull final Posts model)
                    {
                                final String PostKey = getRef(position).getKey();
//                                pd = new ProgressDialog(getContext());
//                                pd.setMessage("Loading Video ..");
//                                pd.show();
                                final String uid=model.getUid();
                                viewHolder.username.setText(model.getName());
                                viewHolder.Date.setText(model.getDate()+" ," +model.getTime());
                                viewHolder.discription.setText(model.getDescription());
                                viewHolder.setPostvideo(getActivity(),model.getPostvideo());
                                Picasso.get().load(model.getProfileimage()).placeholder(R.drawable.profile_image).into(viewHolder.image);
//                                viewHolder.videoView.start();
//                                viewHolder.videoView.requestFocus();
                                viewHolder.LikeButton.setOnClickListener(new View.OnClickListener()
                                {
                            @Override
                            public void onClick(View v) {

                                LikeChecker = true;

                                LikesRef.addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                        if (LikeChecker.equals(true)) {

                                            if (dataSnapshot.child(PostKey).hasChild(currentUserID)) {

                                                LikesRef.child(PostKey).child(currentUserID).removeValue();
                                                LikeChecker = false;


                                            } else {

                                                LikesRef.child(PostKey).child(currentUserID).setValue(true);
                                                LikeChecker = false;


                                            }

                                        }


                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });

                            }
                        });


                        viewHolder.CommentButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {

                                startActivity(new Intent(getContext(), CommentsActivity.class).putExtra("PostKey", PostKey));

                            }
                        });
                        LikesRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                if (dataSnapshot.child(PostKey).hasChild(currentUserId))
                                {

                                 viewHolder.countLikes=(int)  dataSnapshot.child(PostKey).getChildrenCount();

                                    viewHolder.LikeButton.setImageResource(R.drawable.likes);

                                    viewHolder.LikesNo.setText(Integer.toString( viewHolder.countLikes)+(" "));

                                }else
                                {

                                    viewHolder.countLikes=(int)  dataSnapshot.child(PostKey).getChildrenCount();

                                    viewHolder.LikeButton.setImageResource(R.drawable.likeimg);

                                    viewHolder.LikesNo.setText(Integer.toString( viewHolder.countLikes)+(" "));


                                }


                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                        viewHolder.videoView.setOnTouchListener(new View.OnTouchListener()
                        {
                                    @Override
                                    public boolean onTouch(View v, MotionEvent event)
                                    {

                                        if (State.equals("pause"))
                                        {
                                            viewHolder.videoView.start();
                                            viewHolder.videoView.requestFocus();
                                            State = "play";
                                        } else if (State.equals("play"))
                                        {
                                            viewHolder.videoView.pause();
                                            viewHolder.videoView.requestFocus();
                                            State = "pause";
                                        }

                                        return true;
                                    }
                                });

                    }

                    @NonNull
                    @Override
                    public PostsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
                    {
                        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_users_post_layout, parent, false);
                        PostsViewHolder viewHolder = new PostsViewHolder(view);
                        return viewHolder;

                    }
                };
                postList.setAdapter(adapter);
                adapter.startListening();



}





    public static class PostsViewHolder extends RecyclerView.ViewHolder
    {
        View mView;

        ImageView LikeButton,CommentButton,sharebutton,deletebutton;
        TextView LikesNo;
        int countLikes;
        DatabaseReference LikesRef;
        TextView username,Date,Time,discription;
        VideoView videoView;
        CircleImageView image;

        public PostsViewHolder(View itemView)
        {
            super(itemView);
            mView = itemView;

            username=mView.findViewById(R.id.userName);
            videoView=mView.findViewById(R.id.userVideo);

            LikeButton=mView.findViewById(R.id.likeButton);
            CommentButton=mView.findViewById(R.id.commentButton);
            Date=mView.findViewById(R.id.post_date);
            //Time=mView.findViewById(R.id.post_time);
            discription=mView.findViewById(R.id.descrip);
            LikesNo=mView.findViewById(R.id.display_no_of_likes);
            image=mView.findViewById(R.id.userImage);
            //sharebutton=mView.findViewById(R.id.sharpost);
          //  deletebutton=mView.findViewById(R.id.deletpost);

        }

        public void setPostvideo(Context ctx,String postvideo)
        {

            if (postvideo!=null)
            {
                videoView.setVideoURI(Uri.parse(postvideo));

            }

        }


    }
}



