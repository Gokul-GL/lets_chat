package com.example.gl_whatsapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessageViewHolder>
{
    private List<Message> userMessageList;
    private FirebaseAuth mAuth;
    private DatabaseReference UserDatabaseRef;
    private DatabaseReference RootRef;
    private String CurrentUserId,mUser;
    private FirebaseAuth mAthu;

    public MessagesAdapter(List<Message> userMessageList)
    {
         this.userMessageList=userMessageList;
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder
    {

        public TextView SenderMessageText,ReceiverMessageText,MsgDelivered;
        public CircleImageView receiverProfileImage;
        public ImageView messageSenderPicture,messageReciverPicture;


        public MessageViewHolder(@NonNull View itemView)
        {
            super(itemView);
            SenderMessageText=itemView.findViewById(R.id.sender_message_text);
            ReceiverMessageText=itemView.findViewById(R.id.receiver_message_text);
            messageSenderPicture = itemView.findViewById(R.id.message_sender_image_view);
            messageReciverPicture = itemView.findViewById(R.id.message_reciver_image_view);
            receiverProfileImage=itemView.findViewById(R.id.message_profile_image);
            //MsgDelivered=itemView.findViewById(R.id.delivered);

      }
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
     {
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_layout_of_users,viewGroup,false);

        mAuth=FirebaseAuth.getInstance();
        return new MessageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MessageViewHolder messageViewHolder, final int position)
    {

        String messageSenderID=mAuth.getCurrentUser().getUid();
        Message message=userMessageList.get(position);
        String fromUserID=message.getFrom();
        String fromMessageType=message.getType();

        UserDatabaseRef= FirebaseDatabase.getInstance().getReference().child("Users").child(fromUserID);
        UserDatabaseRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {

                if (dataSnapshot.exists())
                {
                    String image=dataSnapshot.child("profileimage").getValue().toString();
                    Picasso.get().load(image).placeholder(R.drawable.profile_image).into(messageViewHolder.receiverProfileImage);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        messageViewHolder.ReceiverMessageText.setVisibility(View.GONE);
        messageViewHolder.receiverProfileImage.setVisibility(View.GONE);
        messageViewHolder.SenderMessageText.setVisibility(View.GONE);
        messageViewHolder.messageReciverPicture.setVisibility(View.GONE);
        messageViewHolder.messageSenderPicture.setVisibility(View.GONE);

        if (fromMessageType.equals("text"))
        {

            if (fromUserID.equals(messageSenderID))
            {
                messageViewHolder.SenderMessageText.setVisibility(View.VISIBLE);
                messageViewHolder.SenderMessageText.setBackgroundResource(R.drawable.sender_message_text_background);
                messageViewHolder.SenderMessageText.setTextColor(Color.WHITE);
                messageViewHolder.SenderMessageText.setGravity(Gravity.LEFT);
                messageViewHolder.SenderMessageText.setText(message.getMessage() + "\n \n" + message.getTime() + " - " + message.getDate());
            }
            else
            {
                messageViewHolder.ReceiverMessageText.setVisibility(View.VISIBLE);
                messageViewHolder.receiverProfileImage.setVisibility(View.VISIBLE);
                messageViewHolder.ReceiverMessageText.setBackgroundResource(R.drawable.reciver_message_text_background);
                messageViewHolder.ReceiverMessageText.setTextColor(Color.WHITE);
                messageViewHolder.ReceiverMessageText.setGravity(Gravity.LEFT);
                messageViewHolder.ReceiverMessageText.setText(message.getMessage() + "\n \n" + message.getTime() + " - " + message.getDate());
            }
        }
        else if (fromMessageType.equals("image"))
        {
            if (fromUserID.equals(messageSenderID))
            {
                messageViewHolder.messageSenderPicture.setVisibility(View.VISIBLE);
                Picasso.get().load(message.getMessage()).into(messageViewHolder.messageSenderPicture);
            }
            else
            {
                messageViewHolder.receiverProfileImage.setVisibility(View.VISIBLE);
                messageViewHolder.messageReciverPicture.setVisibility(View.VISIBLE);
                Picasso.get().load(message.getMessage()).into(messageViewHolder.messageReciverPicture);
            }
        }
        else if (fromMessageType.equals("pdf")) {
            if (fromUserID.equals(messageSenderID)) {
                messageViewHolder.messageSenderPicture.setVisibility(View.VISIBLE);
                Picasso.get()
                        .load("https://firebasestorage.googleapis.com/v0/b/glwhatsapp-e0d11.appspot.com/o/Image%20Files%2Ffile.png?alt=media&Token=b202d79a-fbfe-4383-bc4c-95af52fa4077")
                        .into(messageViewHolder.messageSenderPicture);

            } else {
                messageViewHolder.receiverProfileImage.setVisibility(View.VISIBLE);
                messageViewHolder.messageReciverPicture.setVisibility(View.VISIBLE);
                Picasso.get()
                        .load("https://firebasestorage.googleapis.com/v0/b/glwhatsapp-e0d11.appspot.com/o/Image%20Files%2Ffile.png?alt=media&Token=b202d79a-fbfe-4383-bc4c-95af52fa4077")
                        .into(messageViewHolder.messageReciverPicture);

            }
        }
        else if (fromMessageType.equals("docx")) {
            if (fromUserID.equals(messageSenderID)) {
                messageViewHolder.messageSenderPicture.setVisibility(View.VISIBLE);
                Picasso.get()
                        .load("https://firebasestorage.googleapis.com/v0/b/glwhatsapp-e0d11.appspot.com/o/Image%20Files%2Fmswordimage.png?alt=media&Token=7df7aec6-e1c8-4e2f-98b4-8efe4eaa29a7")
                        .into(messageViewHolder.messageSenderPicture);

            } else {
                messageViewHolder.receiverProfileImage.setVisibility(View.VISIBLE);
                messageViewHolder.messageReciverPicture.setVisibility(View.VISIBLE);
                Picasso.get()
                        .load("https://firebasestorage.googleapis.com/v0/b/glwhatsapp-e0d11.appspot.com/o/Image%20Files%2Fmswordimage.png?alt=media&Token=7df7aec6-e1c8-4e2f-98b4-8efe4eaa29a7")
                        .into(messageViewHolder.messageReciverPicture);

            }
        }
            if (fromUserID.equals(messageSenderID))
            {
                messageViewHolder.itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (userMessageList.get(position).getType().equals("pdf")||userMessageList.get(position).getType().equals("docx"))
                        {
                            CharSequence option[]=new CharSequence[]
                                    {
                                            "Delete For Me",
                                            "Downlode and View This Document",
                                            "Delete For Everyone",
                                            "Cancel"

                                    };
                            AlertDialog.Builder builder=new AlertDialog.Builder(messageViewHolder.itemView.getContext());
                             builder.setTitle("Delete Message ? ");
                             builder.setItems(option, new DialogInterface.OnClickListener()
                             {
                                 @Override
                                 public void onClick(DialogInterface dialog, int which)
                                 {
                                    if (which==0)
                                    {
                                        deleteSendMessage(position,messageViewHolder);
                                        Intent intent=new Intent(messageViewHolder.itemView.getContext(),MainActivity.class);
                                        messageViewHolder.itemView.getContext().startActivity(intent);

                                    }
                                    else if (which==1)
                                    {
                                        Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(userMessageList.get(position).getMessage()));
                                        messageViewHolder.itemView.getContext().startActivity(intent);

                                    }
                                    else if (which==2)
                                    {
                                        deleteMessageEveryOne(position,messageViewHolder);
                                        Intent intent=new Intent(messageViewHolder.itemView.getContext(),MainActivity.class);
                                        messageViewHolder.itemView.getContext().startActivity(intent);
                                    }
                                 }
                             });
                             builder.show();
                        }
                        else if (userMessageList.get(position).getType().equals("text"))
                        {
                            CharSequence option[]=new CharSequence[]
                                    {
                                            "Delete For Me",
                                            "Delete For Everyone",
                                            "Cancel"

                                    };
                            AlertDialog.Builder builder=new AlertDialog.Builder(messageViewHolder.itemView.getContext());
                            builder.setTitle("Delete Message ? ");
                            builder.setItems(option, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    if (which==0)
                                    {
                                        deleteSendMessage(position,messageViewHolder);
                                       Intent intent=new Intent(messageViewHolder.itemView.getContext(),MainActivity.class);
                                       messageViewHolder.itemView.getContext().startActivity(intent);
                                    }
                                    else if (which==1)
                                    {
                                        deleteMessageEveryOne(position,messageViewHolder);
                                       Intent intent=new Intent(messageViewHolder.itemView.getContext(),MainActivity.class);
                                       messageViewHolder.itemView.getContext().startActivity(intent);

                                    }
                                }
                            });
                            builder.show();
                        }
                        else if (userMessageList.get(position).getType().equals("image"))
                        {
                            CharSequence option[]=new CharSequence[]
                                    {
                                            "Delete For Me",
                                            "View this image",
                                            "Delete For Everyone",
                                            "Cancel"

                                    };
                            AlertDialog.Builder builder=new AlertDialog.Builder(messageViewHolder.itemView.getContext());
                            builder.setTitle("Delete Message ? ");
                            builder.setItems(option, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    if (which==0)
                                    {
                                        deleteSendMessage(position,messageViewHolder);
                                      Intent intent=new Intent(messageViewHolder.itemView.getContext(),MainActivity.class);
                                       messageViewHolder.itemView.getContext().startActivity(intent);
                                    }
                                    else if (which==1)
                                    {
                                        Intent intent=new Intent(messageViewHolder.itemView.getContext(),ImageViewerActivity.class);
                                        intent.putExtra("url",userMessageList.get(position).getMessage());
                                        messageViewHolder.itemView.getContext().startActivity(intent);
                                    }
                                    else if (which==2)
                                    {
                                        deleteMessageEveryOne(position,messageViewHolder);
                                        Intent intent=new Intent(messageViewHolder.itemView.getContext(),MainActivity.class);
                                        messageViewHolder.itemView.getContext().startActivity(intent);
                                    }
                                }
                            });
                            builder.show();
                        }

                    }
                });
            }
            else
            {
                messageViewHolder.itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (userMessageList.get(position).getType().equals("pdf")||userMessageList.get(position).getType().equals("docx"))
                        {
                            CharSequence option[]=new CharSequence[]
                                    {
                                            "Delete For Me",
                                            "Downlode and View This Document",
                                            "Cancel"

                                    };
                            AlertDialog.Builder builder=new AlertDialog.Builder(messageViewHolder.itemView.getContext());
                            builder.setTitle("Delete Message ? ");
                            builder.setItems(option, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    if (which==0)
                                    {
                                        deleteRecivedMessage(position,messageViewHolder);
                                        Intent intent=new Intent(messageViewHolder.itemView.getContext(),MainActivity.class);
                                        messageViewHolder.itemView.getContext().startActivity(intent);
                                    }
                                    else if (which==1)
                                    {
                                        Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(userMessageList.get(position).getMessage()));
                                        messageViewHolder.itemView.getContext().startActivity(intent);

                                    }
                                }
                            });
                            builder.show();
                        }
                        else if (userMessageList.get(position).getType().equals("text"))
                        {
                            CharSequence option[]=new CharSequence[]
                                    {
                                            "Delete For Me",
                                            "Cancel"

                                    };
                            AlertDialog.Builder builder=new AlertDialog.Builder(messageViewHolder.itemView.getContext());
                            builder.setTitle("Delete Message ? ");
                            builder.setItems(option, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    if (which==0)
                                    {
                                        deleteRecivedMessage(position,messageViewHolder);
                                        Intent intent=new Intent(messageViewHolder.itemView.getContext(),MainActivity.class);
                                        messageViewHolder.itemView.getContext().startActivity(intent);
                                    }

                                }
                            });
                            builder.show();
                        }
                        else if (userMessageList.get(position).getType().equals("image"))
                        {
                            CharSequence option[]=new CharSequence[]
                                    {
                                            "Delete For Me",
                                            "View this image",
                                            "Cancel",

                                    };
                            AlertDialog.Builder builder=new AlertDialog.Builder(messageViewHolder.itemView.getContext());
                            builder.setTitle("Delete Message ? ");
                            builder.setItems(option, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    if (which==0)
                                    {
                                        deleteRecivedMessage(position,messageViewHolder);
                                        Intent intent=new Intent(messageViewHolder.itemView.getContext(),MainActivity.class);
                                        messageViewHolder.itemView.getContext().startActivity(intent);



                                    }
                                    else if (which==1)
                                    {
                                        Intent intent=new Intent(messageViewHolder.itemView.getContext(),ImageViewerActivity.class);
                                        intent.putExtra("url",userMessageList.get(position).getMessage());
                                        messageViewHolder.itemView.getContext().startActivity(intent);
                                    }

                                }
                            });
                            builder.show();
                        }

                    }
                });
            }


    }

    @Override
    public int getItemCount()
    {
        return userMessageList.size();
    }
    public void deleteSendMessage(final int position,final MessageViewHolder holder)
    {
        DatabaseReference rootRef=FirebaseDatabase.getInstance().getReference();
         rootRef.child("Message").child(userMessageList.get(position).getFrom())
                 .child(userMessageList.get(position).getTo())
                 .child(userMessageList.get(position).getMessageID())
                 .removeValue().addOnCompleteListener(new OnCompleteListener<Void>()
         {
             @Override
             public void onComplete(@NonNull Task<Void> task)
             {
                 if (task.isSuccessful())
                 {
                     Toast.makeText(holder.itemView.getContext(), "Deleted Successfully.. ", Toast.LENGTH_SHORT).show();
                 }
                 else
                 {
                     Toast.makeText(holder.itemView.getContext(), "Error Occurred ..", Toast.LENGTH_SHORT).show();
                 }

             }
         });

    }


    public void deleteRecivedMessage(final int position,final MessageViewHolder holder)
    {
        DatabaseReference rootRef=FirebaseDatabase.getInstance().getReference();
        rootRef.child("Message").child(userMessageList.get(position).getTo())
                .child(userMessageList.get(position).getFrom())
                .child(userMessageList.get(position).getMessageID())
                .removeValue().addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if (task.isSuccessful())
                {
                    Toast.makeText(holder.itemView.getContext(), "Deleted Successfully.. ", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(holder.itemView.getContext(), "Error Occurred ..", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }



    public void deleteMessageEveryOne(final int position,final MessageViewHolder holder)
    {
        final DatabaseReference rootRef=FirebaseDatabase.getInstance().getReference();
        rootRef.child("Message").child(userMessageList.get(position).getTo())
                .child(userMessageList.get(position).getFrom())
                .child(userMessageList.get(position).getMessageID())
                .removeValue().addOnCompleteListener(new OnCompleteListener<Void>()
        {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if (task.isSuccessful())
                {

                    rootRef.child("Message").child(userMessageList.get(position).getFrom())
                            .child(userMessageList.get(position).getTo())
                            .child(userMessageList.get(position).getMessageID())
                            .removeValue().addOnCompleteListener(new OnCompleteListener<Void>()
                    {
                        @Override
                        public void onComplete(@NonNull Task<Void> task)
                        {
                            if (task.isSuccessful())
                            {

                                Toast.makeText(holder.itemView.getContext(), "Deleted Successfully.. ", Toast.LENGTH_SHORT).show();

                            }

                        }
                    });



                }
                else
                {
                    Toast.makeText(holder.itemView.getContext(), "Error Occurred ..", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

}
