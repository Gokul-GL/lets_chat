package com.example.gl_whatsapp;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class FindFriendsActivity extends AppCompatActivity
{
    private Toolbar mToolbar;
    private RecyclerView FindFriendsRecyclarList;
    private DatabaseReference UserRef;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friends);

        UserRef= FirebaseDatabase.getInstance().getReference().child("Users");

        FindFriendsRecyclarList=findViewById(R.id.find_friends_recycler_list);
        FindFriendsRecyclarList.setLayoutManager(new LinearLayoutManager(this));
        mToolbar=findViewById(R.id.find_friends_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Find Friends");

    }

    @Override
    protected void onStart()
    {
        super.onStart();
        FirebaseRecyclerOptions<Contacts> options=new
                FirebaseRecyclerOptions.Builder<Contacts>()
                .setQuery(UserRef,Contacts.class)
                .build();

        FirebaseRecyclerAdapter<Contacts,FindFriendsViewHolder> adapter= new FirebaseRecyclerAdapter<Contacts, FindFriendsViewHolder>(options){
            @Override
            protected void onBindViewHolder(@NonNull FindFriendsViewHolder holder, final int position, @NonNull Contacts model)
            {
                holder.username.setText(model.getName());
                holder.userStatus.setText(model.getStatus());
                Picasso.get().load(model.getProfileimage()).placeholder(R.drawable.profile_image).into(holder.profileimage);

                holder.itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        String visit_user_id=getRef(position).getKey();
                        Intent profileintent=new Intent(FindFriendsActivity.this,ProfileActivity.class);
                        profileintent.putExtra("visit_user_id",visit_user_id);
                        startActivity(profileintent);

                    }
                });


            }

            @NonNull
            @Override
            public FindFriendsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
            {
                View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_display_layout,viewGroup,false);
                FindFriendsViewHolder viewHolder=new FindFriendsViewHolder(view);
                return viewHolder;
            }
        };
        FindFriendsRecyclarList.setAdapter(adapter);
        adapter.startListening();
    }

    public static class FindFriendsViewHolder extends RecyclerView.ViewHolder
    {

        TextView username,userStatus;
        CircleImageView profileimage;
        ImageButton callbutton;
        public FindFriendsViewHolder(@NonNull View itemView)
        {
            super(itemView);
            username=itemView.findViewById(R.id.user_profile_name);
            userStatus=itemView.findViewById(R.id.user_status);
            profileimage=itemView.findViewById(R.id.users_profile_image);
            callbutton=itemView.findViewById(R.id.imageButton);
            callbutton.setVisibility(View.INVISIBLE);
        }
    }

}
