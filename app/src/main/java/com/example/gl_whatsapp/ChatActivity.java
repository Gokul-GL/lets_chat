package com.example.gl_whatsapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gl_whatsapp.notification.APIServices;
import com.example.gl_whatsapp.notification.Client;
import com.example.gl_whatsapp.notification.Data;
import com.example.gl_whatsapp.notification.Response;
import com.example.gl_whatsapp.notification.Sender;
import com.example.gl_whatsapp.notification.Token;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;

public class ChatActivity extends AppCompatActivity
{

    private Toolbar mToolbar;
    private ImageView SendMessageButton,SendFileButton;
    private EditText userMessagesInput;
    private TextView userName,userLastSeen;
    private RecyclerView usermessageList;
    private final List<Message> messageList=new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MessagesAdapter messagesAdapter;
    private CircleImageView userimage;
    private String messageResiverID,messageResiverName,messageResiverImage,messageSenderID,hisUID;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private DatabaseReference RootRef,userRefforSeen;
    private static final int Gallery_Pick=1;
    private ProgressDialog progressDialog;
    private String CurrentUserId;
    private String SaveCurrentTime,SaveCurrentDate;
    private String cheker="",myUrl="";
    private StorageTask Uplodetask;
    private Uri fileUri;
    APIServices apiServices;
    boolean notify=false;
    private ValueEventListener seenListener;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        mAuth=FirebaseAuth.getInstance();
        progressDialog=new ProgressDialog(this);
        RootRef= FirebaseDatabase.getInstance().getReference();
        InitializeController();
        apiServices=Client.getRetrofit("https://fcm.googleapis.com/").create(APIServices.class);
        Intent intent=getIntent();
        Bundle b=intent.getExtras();
        hisUID=b.getString("hisUID");
        messageSenderID=mAuth.getCurrentUser().getUid();
        messageResiverID=getIntent().getExtras().get("visit_user_id").toString();
        messageResiverName=getIntent().getExtras().get("visit_user_name").toString();
        messageResiverImage=getIntent().getExtras().get("visit_image").toString();
        userName.setText(messageResiverName);
        userLastSeen.setText("Last Seen");
        Picasso.get().load(messageResiverImage).placeholder(R.drawable.profile_image).into(userimage );
       // Toast.makeText(this, messageResiverID, Toast.LENGTH_SHORT).show();
        Toast.makeText(this, messageResiverName, Toast.LENGTH_SHORT).show();




        //hisUID=intent.getStringExtra("hisUID");


        SendMessageButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                notify=true;
                SendMessage();
            }
        });
        FetcheMessage();
        SendFileButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {


                CharSequence Options[]=new CharSequence[]
                        {
                                "Images",
                                "PDF Files",
                                "MS Word Files"
                        };

                AlertDialog.Builder builder=new AlertDialog.Builder(ChatActivity.this);
                builder.setTitle(" Select The Files ");
                builder.setItems(Options, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (which==0)
                        {
                            cheker="image";
                            Intent galleryIntent= new Intent();
                            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                            galleryIntent.setType("image/*");
                            startActivityForResult(galleryIntent.createChooser(galleryIntent,"Select Image"),438);
                        }
                        if (which==1)
                        {
                            cheker="pdf";
                            Intent galleryIntent= new Intent();
                            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                            galleryIntent.setType("application/pdf");
                            startActivityForResult(galleryIntent.createChooser(galleryIntent,"Select PDF File"),438);
                        }
                        if (which==2)
                        {
                            cheker="docx";
                            Intent galleryIntent= new Intent();
                            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                            galleryIntent.setType("application/msword");
                            startActivityForResult(galleryIntent.createChooser(galleryIntent,"Select Word File"),438);
                        }
                    }
                });
                builder.show();
            }
        });


        DisplyLasteSeen();
        //seenMessage();
    }
    private void updateUserStatus(String state)
    {
        mAuth=FirebaseAuth.getInstance();
        CurrentUserId=mAuth.getCurrentUser().getUid();
        String SaveCurrentTime,SaveCurrentDate;
        Calendar calForData= Calendar.getInstance();
        SimpleDateFormat currentDateFormat=new SimpleDateFormat("MMM dd,yyyy");
        SaveCurrentDate=currentDateFormat.format(calForData.getTime());

        Calendar calForTime= Calendar.getInstance();
        SimpleDateFormat currentTimeFormat=new SimpleDateFormat("hh:mm a");
        SaveCurrentTime=currentTimeFormat.format(calForTime.getTime());


        HashMap<String,Object> onlineStateMap=new HashMap<>();
        onlineStateMap.put("time",SaveCurrentTime);
        onlineStateMap.put("date",SaveCurrentDate);
        onlineStateMap.put("state",state);
        CurrentUserId=mAuth.getCurrentUser().getUid();
        RootRef.child("Users").child(CurrentUserId).child("userState")
                .updateChildren(onlineStateMap);


    }
    @Override
    protected void onStart()
    {
        super.onStart();

        updateUserStatus("online");

    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        updateUserStatus("online");
    }
    private void FetcheMessage()
    {
         RootRef.child("Message").child(messageSenderID).child(messageResiverID).addChildEventListener(new ChildEventListener()
         {
             @Override
             public void onChildAdded(DataSnapshot dataSnapshot, String s)
             {
                 if (dataSnapshot.exists())
                 {
                     Message message=dataSnapshot.getValue(Message.class);
                     messageList.add(message );
                     messagesAdapter.notifyDataSetChanged();
                     usermessageList.smoothScrollToPosition(usermessageList.getAdapter().getItemCount());
                 }


             }

             @Override
             public void onChildChanged(DataSnapshot dataSnapshot, String s)
             {

             }

             @Override
             public void onChildRemoved(DataSnapshot dataSnapshot)
             {

             }

             @Override
             public void onChildMoved(DataSnapshot dataSnapshot, String s)
             {

             }

             @Override
             public void onCancelled(DatabaseError databaseError)
             {

             }
         });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==438 && resultCode == RESULT_OK && data!=null && data.getData()!=null)
        {
            progressDialog.setTitle("Sending File");
             progressDialog.setMessage("Please Wait ...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();



            fileUri=data.getData();
            if (!cheker.equals("image"))
            {
                StorageReference MessageImageStorageRef= FirebaseStorage.getInstance().getReference().child("Document Files");

                final String message_sender_ref="Message/"+messageSenderID+"/"+messageResiverID;
                final String message_resevier_ref="Message/"+messageResiverID+"/"+messageSenderID;

                DatabaseReference user_message_key=RootRef.child("Message").child(messageSenderID).child(messageResiverID).push();
                final String message_Push_ID=user_message_key.getKey();
                final StorageReference filePath=MessageImageStorageRef.child(message_Push_ID + "." + cheker);
                filePath.putFile(fileUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>()
                {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task)
                    {
                     if (task.isSuccessful())
                     {
                         Map messageTextBody=new HashMap();
                         messageTextBody.put("from",messageSenderID);
                         messageTextBody.put("message",Objects.requireNonNull(task.getResult()).getStorage().getDownloadUrl().toString());
                         messageTextBody.put("name",fileUri.getLastPathSegment());
                         messageTextBody.put("date",SaveCurrentDate);
                         messageTextBody.put("time",SaveCurrentTime);
                         messageTextBody.put("type",cheker);
                         messageTextBody.put("messageID",message_Push_ID);
                         messageTextBody.put("to",messageResiverID);




                         Map messageBodyDetailes=new HashMap();
                         messageBodyDetailes.put(message_sender_ref + "/" + message_Push_ID, messageTextBody);
                         messageBodyDetailes.put(message_resevier_ref + "/" + message_Push_ID, messageTextBody);

                         RootRef.updateChildren(messageBodyDetailes);
                          progressDialog.dismiss();


                     }
                    }
                }).addOnFailureListener(new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception e)
                    {
                        progressDialog.dismiss();
                        Toast.makeText(ChatActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>()
                {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot)
                    {
                        double p=(100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                        progressDialog.setMessage((int)p+ " % Uploding .....");

                    }
                });
            }
           else if (cheker.equals("image"))
            {
                StorageReference MessageImageStorageRef= FirebaseStorage.getInstance().getReference().child("Image Files");

                final String message_sender_ref="Message/"+messageSenderID+"/"+messageResiverID;
                final String message_resevier_ref="Message/"+messageResiverID+"/"+messageSenderID;

                DatabaseReference user_message_key=RootRef.child("Message").child(messageSenderID).child(messageResiverID).push();
                final String message_Push_ID=user_message_key.getKey();
                final StorageReference filePath=MessageImageStorageRef.child(message_Push_ID + "." + "jpg");
                Uplodetask=filePath.putFile(fileUri);
                Uplodetask.continueWithTask(new Continuation()
                {
                    @Override
                    public Object then(@NonNull Task task) throws Exception
                    {
                        if (!task.isSuccessful())
                        {
                            throw task.getException();
                        }
                        return filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task)
                    {
                        if (task.isSuccessful())
                        {
                            Uri downlodeUrl=task.getResult();
                             myUrl=downlodeUrl.toString();
                            Map messageTextBody=new HashMap();
                            messageTextBody.put("from",messageSenderID);
                            messageTextBody.put("message",myUrl);
                            messageTextBody.put("name",fileUri.getLastPathSegment());
                            messageTextBody.put("date",SaveCurrentDate);
                            messageTextBody.put("time",SaveCurrentTime);
                            messageTextBody.put("type",cheker);
                            messageTextBody.put("messageID",message_Push_ID);
                            messageTextBody.put("to",messageResiverID);



                            Map messageBodyDetailes=new HashMap();
                            messageBodyDetailes.put(message_sender_ref + "/" + message_Push_ID, messageTextBody);
                            messageBodyDetailes.put(message_resevier_ref + "/" + message_Push_ID, messageTextBody);


                            RootRef.updateChildren(messageBodyDetailes).addOnCompleteListener(new OnCompleteListener()
                            {
                                @Override
                                public void onComplete(@NonNull Task task)
                                {
                                    if (task.isSuccessful())
                                    {

                                        progressDialog.dismiss();
                                        Toast.makeText(ChatActivity.this, "Message Send Successfuly..", Toast.LENGTH_SHORT).show();


                                    }
                                    else
                                    {
                                        progressDialog.dismiss();
                                        String message=task.getException().toString();
                                        Toast.makeText(ChatActivity.this, "Error :"+message, Toast.LENGTH_SHORT).show();
                                    }
                                    userMessagesInput.setText("");

                                }
                            });


                        }

                    }
                });

            }
            else
            {
                progressDialog.dismiss();
                Toast.makeText(this, "Nothing Selected , Error ..", Toast.LENGTH_SHORT).show();

            }
        }
    }
  private void SendMessage()
    {
        final String messageText=userMessagesInput.getText().toString();

        if (TextUtils.isEmpty(messageText))
        {
            Toast.makeText(this, "Please Write a Message..", Toast.LENGTH_SHORT).show();
        }
        else
        {

            String message_sender_ref="Message/"+messageSenderID+"/"+messageResiverID;
            String message_resevier_ref="Message/"+messageResiverID+"/"+messageSenderID;

            DatabaseReference user_message_key=RootRef.child("Message").child(messageSenderID).child(messageResiverID).push();
            String message_Push_ID=user_message_key.getKey();

            Map messageTextBody=new HashMap();
            messageTextBody.put("from",messageSenderID);
            messageTextBody.put("message",messageText);
            messageTextBody.put("date",SaveCurrentDate);
            messageTextBody.put("time",SaveCurrentTime);
            messageTextBody.put("type","text");
            messageTextBody.put("messageID",message_Push_ID);
            messageTextBody.put("to",messageResiverID);
//            messageTextBody.put("isSeen",false);
          Map messageBodyDetailes=new HashMap();
          messageBodyDetailes.put(message_sender_ref + "/" + message_Push_ID, messageTextBody);
          messageBodyDetailes.put(message_resevier_ref + "/" + message_Push_ID, messageTextBody);


          RootRef.updateChildren(messageBodyDetailes).addOnCompleteListener(new OnCompleteListener()
          {
              @Override
              public void onComplete(@NonNull Task task)
              {
                  if (task.isSuccessful())
                  {
                      Toast.makeText(ChatActivity.this, "Message Send Successfuly..", Toast.LENGTH_SHORT).show();
                      userMessagesInput.setText("");
                      //seenMessage();
                  }
                  else
                  {
                      String message=task.getException().toString();
                      Toast.makeText(ChatActivity.this, "Error :"+message, Toast.LENGTH_SHORT).show();
                  }
              }
          });

            final String msg=messageText;
            currentUser=FirebaseAuth.getInstance().getCurrentUser();
            final DatabaseReference database=RootRef.child("Users").child(currentUser.getUid());
            database.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {

                    Message user=dataSnapshot.getValue(Message.class);
                    if (notify)
                    {
                        sendNotification(messageResiverID,user.getName(),msg);
                        Toast.makeText(ChatActivity.this, "Notification", Toast.LENGTH_SHORT).show();
                    }
                    notify=false;
                }
                @Override
                public void onCancelled(DatabaseError databaseError)
                {

                }
            });
        }
    }
//    private void seenMessage()
//    {
//        final String message_sender_ref="Message/"+messageSenderID+"/"+messageResiverID;
//        final String message_resevier_ref="Message/"+messageResiverID+"/"+messageSenderID;
//        userRefforSeen=RootRef.child("Message").child(messageSenderID).child(messageResiverID).push();
//        final String message_Push_ID=userRefforSeen.getKey();
//        seenListener=userRefforSeen.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                for (DataSnapshot ds:dataSnapshot.getChildren())
//                {
//                    Message msg=ds.getValue(Message.class);
//                    if (msg.getTo().equals(currentUser.getUid()) && msg.getFrom().equals(messageSenderID));
//                    {
//                        Map hasSeenHashMap=new HashMap();
//                        hasSeenHashMap.put("isSeen",true);
//                        Map messageBodyDetailes=new HashMap();
//
//                        messageBodyDetailes.put(message_sender_ref + "/" + message_Push_ID, hasSeenHashMap);
//                        messageBodyDetailes.put(message_resevier_ref + "/" + message_Push_ID, hasSeenHashMap);
//                        RootRef.updateChildren(messageBodyDetailes);
//
//                    }
//
//                }
//            }
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }
//    @Override
//    protected void onPause()
//    {
//        super.onPause();
//        userRefforSeen.removeEventListener(seenListener);
//    }

    private void sendNotification(final String messageResiverID, final String name, final String messageText)
    {
        Intent intent=getIntent();
        Bundle b=intent.getExtras();
        hisUID=b.getString("hisUID");
        DatabaseReference allTokens=FirebaseDatabase.getInstance().getReference("Tokens");
        Query query=allTokens.orderByValue().equalTo(messageResiverID);
        query.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
               for (DataSnapshot ds:dataSnapshot.getChildren())
               {
                   Token token=ds.getValue(Token.class);
                   Data data=new Data(currentUser.getUid(),name+":"+messageText,"New Message",hisUID, android.R.drawable.sym_def_app_icon);
                   Sender  sender=new Sender(data,token.getToken());
                   apiServices.sendNotification(sender)
                           .enqueue(new Callback<Response>()
                           {
                               @Override
                               public void onResponse(Call<Response> call, retrofit2.Response<Response> response)
                               {

                               }
                               @Override
                               public void onFailure(Call<Response> call, Throwable t)
                               {

                               }
                           });
               }
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {
            }
        });
    }
    @SuppressLint("WrongViewCast")
    private void InitializeController()
    {
        mToolbar=findViewById(R.id.chat_toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        LayoutInflater layoutInflater= (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView=layoutInflater.inflate(R.layout.custom_chat_bar,null);
        actionBar.setCustomView(actionBarView);
        userName=findViewById(R.id.custom_profile_name);
        userimage=findViewById(R.id.custom_profile_image);
        userLastSeen=findViewById(R.id.custom_user_last_seen);
        SendMessageButton=findViewById(R.id.send_message_button);
        SendFileButton=findViewById(R.id.send_image_button);
        userMessagesInput=findViewById(R.id.input_message);
        usermessageList=findViewById(R.id.myMessageList);
        messagesAdapter=new MessagesAdapter(messageList);
        linearLayoutManager=new LinearLayoutManager(this);
        usermessageList.setHasFixedSize(true);
        usermessageList.setLayoutManager(linearLayoutManager);
        usermessageList.setAdapter(messagesAdapter);
        Calendar calForData= Calendar.getInstance();
        SimpleDateFormat currentDateFormat=new SimpleDateFormat("MMM dd,yyyy");
        SaveCurrentDate=currentDateFormat.format(calForData.getTime());
        Calendar calForTime= Calendar.getInstance();
        SimpleDateFormat currentTimeFormat=new SimpleDateFormat("hh:mm a");
        SaveCurrentTime=currentTimeFormat.format(calForTime.getTime());
    }
    private void DisplyLasteSeen()
    {
        RootRef.child("Users").child(messageResiverID)
                .addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {

                        if (dataSnapshot.child("userState").hasChild("state"))
                        {
                            String state=dataSnapshot.child("userState").child("state").getValue().toString();
                            String date=dataSnapshot.child("userState").child("date").getValue().toString();
                            String time=dataSnapshot.child("userState").child("time").getValue().toString();

                            if (state.equals("online"))
                            {
                                userLastSeen.setText("online");
                            }
                            else if (state.equals("offline"))
                            {
                                userLastSeen.setText("Last Seen :" + date +" "+ time);
                            }
                        }
                        else
                        {
                            userLastSeen.setText("offline");
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
    }
}
