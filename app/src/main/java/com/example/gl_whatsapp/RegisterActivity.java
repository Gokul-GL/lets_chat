package com.example.gl_whatsapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Calendar;

public class RegisterActivity extends AppCompatActivity
{
    private Button CreateAccountButton;
    private EditText useremail,userpassword;
    private TextView AlreadyHaveAccount;
    private FirebaseAuth mAthu;
    private ProgressDialog LodingBar;
    private DatabaseReference RootRef;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAthu=FirebaseAuth.getInstance();
        RootRef= FirebaseDatabase.getInstance().getReference();

        InitalizeFields();

        AlreadyHaveAccount.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                    SendUserToLoginActivity();
            }
        });

        CreateAccountButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                CreateNewAccount();
            }

            private void CreateNewAccount()
            {
                String email=useremail.getText().toString();
                String password=userpassword.getText().toString();
                if (TextUtils.isEmpty(email))
                {
                    Toast.makeText(RegisterActivity.this, "Please Enter a Email ...", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(password))
                {
                    Toast.makeText(RegisterActivity.this, "Please Enter a password ...", Toast.LENGTH_SHORT).show();
                }
                if(password.length()<6)
                {
                    Toast.makeText(RegisterActivity.this, "Password shoud be atleast 6 characters", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    LodingBar.setTitle("Createing New Account");
                    LodingBar.setMessage("Please Wait..");
                    LodingBar.setCanceledOnTouchOutside(true);
                    LodingBar.show();
                    mAthu.createUserWithEmailAndPassword(email,password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>()
                            {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task)
                                {
                                    if (task.isComplete())
                                    {
                                        String deviceToken= FirebaseInstanceId.getInstance().getToken();
                                        String currentUserID=mAthu.getCurrentUser().getUid();
                                        RootRef.child("Users").child(currentUserID).setValue("");
                                        RootRef.child("Users").child(currentUserID).child("device_token")
                                        .setValue(deviceToken);
                                        SendEmailVerificationMessage();
                                        LodingBar.dismiss();
                                    }
                                    else
                                    {
                                        String message=task.getException().toString();
                                        Toast.makeText(RegisterActivity.this, "Error:"+message, Toast.LENGTH_SHORT).show();
                                        LodingBar.dismiss();
                                    }
                                }
                            });
                }
            }
        });
    }


    private void InitalizeFields()
    {
        CreateAccountButton=findViewById(R.id.create_button);
        useremail=findViewById(R.id.create_editText);
        userpassword=findViewById(R.id.create_editText3);
        AlreadyHaveAccount=findViewById(R.id.textView3);
        LodingBar=new ProgressDialog(this);
    }
    private void SendUserToLoginActivity()
    {
        Intent Loginintent=new Intent(RegisterActivity.this,LoginActivity.class);
        startActivity(Loginintent);

    }
    private void SendEmailVerificationMessage()
    {
        FirebaseUser user=mAthu.getCurrentUser();
        if (user!=null)
        {
            user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>()
            {
                @Override
                public void onComplete(@NonNull Task<Void> task)
                {
                    if (task.isSuccessful())
                    {
                        Toast.makeText(RegisterActivity.this, "Registration Successfully,we ' ve send youa mail . please check and verify your mail.. ", Toast.LENGTH_LONG).show();
                        SendUserToLoginActivity();
                        mAthu.signOut();

                    }
                    else
                    {
                        String message=task.getException().toString();
                        Toast.makeText(RegisterActivity.this, " Error: " +message, Toast.LENGTH_SHORT).show();
                        mAthu.signOut();

                    }

                }
            });
        }
    }

}
