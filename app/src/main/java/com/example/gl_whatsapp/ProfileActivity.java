package com.example.gl_whatsapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity
{
    private  String receiverUserId,Current_state,sendUserID;
    private CircleImageView userprofileimage;
    private TextView userprofileName,usersprofileStatus;
    private Button sendmessagerequstButton,DeclineMessageRequstButton;
    private DatabaseReference UserRef,ChatRequstRef,ContactsRef,notificationRef;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        mAuth=FirebaseAuth.getInstance();
        UserRef= FirebaseDatabase.getInstance().getReference().child("Users");
        ChatRequstRef= FirebaseDatabase.getInstance().getReference().child("Chat Requests");
        ContactsRef= FirebaseDatabase.getInstance().getReference().child("Contacts");
        notificationRef= FirebaseDatabase.getInstance().getReference().child("Notifications");
        receiverUserId=getIntent().getExtras().get("visit_user_id").toString();
        sendUserID=mAuth.getCurrentUser().getUid();
      //  Toast.makeText(this, "UserId :"+receiverUserId, Toast.LENGTH_SHORT).show();

        userprofileimage=findViewById(R.id.visit_profile_image);
        userprofileName=findViewById(R.id.visit_user_name);
        usersprofileStatus=findViewById(R.id.visit_profile_status);
        sendmessagerequstButton=findViewById(R.id.send_message_requst_button);
        DeclineMessageRequstButton=findViewById(R.id.decline_message_requst_button);
        Current_state ="new";

        RetriveUserInformation();
        ManageChatRequests();

    }

    private void RetriveUserInformation()
    {
        UserRef.child(receiverUserId).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
             if ((dataSnapshot.exists())&&(dataSnapshot.hasChild("profileimage")))
             {

                 String userimage=dataSnapshot.child("profileimage").getValue().toString();
                 String username=dataSnapshot.child("name").getValue().toString();
                 String userstatus=dataSnapshot.child("status").getValue().toString();
                 Picasso.get().load(userimage).placeholder(R.drawable.profile_image).into(userprofileimage);
                 userprofileName.setText(username);
                 usersprofileStatus.setText(userstatus);

                 ManageChatRequests();

             }
             else
             {
                 String username=dataSnapshot.child("name").getValue().toString();
                 String userstatus=dataSnapshot.child("status").getValue().toString();
                 userprofileName.setText(username);
                 usersprofileStatus.setText(userstatus);

                 ManageChatRequests();


            }

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

    }
   private void ManageChatRequests()
  {
        ChatRequstRef.child(sendUserID)
                .addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        if (dataSnapshot.hasChild(receiverUserId))
                        {
                            String request_type=dataSnapshot.child(receiverUserId).child("request_type").getValue().toString();
                            if (request_type.equals("sent"))
                            {
                                Current_state="request_sent";
                                sendmessagerequstButton.setText("Cancel Chat Request");
                            }
                            else if (request_type.equals("received"))
                            {
                                Current_state="request_received";
                                sendmessagerequstButton.setText("Accept Chat Request");
                                DeclineMessageRequstButton.setVisibility(View.VISIBLE);
                                DeclineMessageRequstButton.setEnabled(true);
                                DeclineMessageRequstButton.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        CancelChatRequest();
                                    }
                                });
                            }

                        }
                      else
                       {
                            ContactsRef.child(sendUserID)
                                    .addValueEventListener(new ValueEventListener()
                                    {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot)
                                        {
                                            if (dataSnapshot.hasChild(receiverUserId))
                                            {
                                                Current_state = "Friends";
                                                sendmessagerequstButton.setText("Remove this Contact");
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError)
                                        {

                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
       if (!sendUserID.equals(receiverUserId))
     {
            sendmessagerequstButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    sendmessagerequstButton.setEnabled(false);

                    if (Current_state.equals("new"))
                    {
                        SendChatRequst();
                    }
                    if (Current_state.equals("request_sent"))
                    {
                        //RemoveSpacificationContact();
                        CancelChatRequest();
                    }
                    if (Current_state.equals("request_received"))
                    {
                      AcceptChatRequest();
                    }
                    if (Current_state.equals("Friends"))
                    {
                       // CancelChatRequest();
                       RemoveSpacificationContact();
                    }
                }
            });
        }
        else
        {
            sendmessagerequstButton.setVisibility(View.INVISIBLE);
        }



    }
   private void RemoveSpacificationContact()
    {
        ContactsRef.child(sendUserID).child(receiverUserId)
                .removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        if (task.isSuccessful())
                        {
                            ContactsRef.child(receiverUserId).child(sendUserID)
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>()
                                    {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if (task.isSuccessful())
                                            {
                                                sendmessagerequstButton.setEnabled(true);
                                                Current_state="new";
                                                sendmessagerequstButton.setText("Send Message");
                                                DeclineMessageRequstButton.setVisibility(View.INVISIBLE);
                                                DeclineMessageRequstButton.setEnabled(false);
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void AcceptChatRequest()
    {
        ContactsRef.child(sendUserID).child(receiverUserId)
                .child("Contacts").setValue("Saved")
                .addOnCompleteListener(new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                       if (task.isSuccessful())
                       {
                        ContactsRef.child(receiverUserId).child(sendUserID)
                                   .child("Contacts").setValue("Saved")
                                   .addOnCompleteListener(new OnCompleteListener<Void>()
                                   {
                                       @Override
                                       public void onComplete(@NonNull Task<Void> task)
                                       {
                                           if (task.isSuccessful())
                                           {
                                          ChatRequstRef.child(sendUserID).child(receiverUserId)
                                                    .removeValue()
                                                    .addOnCompleteListener(new OnCompleteListener<Void>()
                                                    {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task)
                                                        {
                                                            if (task.isSuccessful())
                                                            {
                                                            ChatRequstRef.child(receiverUserId).child(sendUserID)
                                                                        .removeValue()
                                                                        .addOnCompleteListener(new OnCompleteListener<Void>()
                                                                        {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task)
                                                                            {

                                                                                    sendmessagerequstButton.setEnabled(true);
                                                                                    Current_state="Friends";
                                                                                    sendmessagerequstButton.setText("Remove This Contact ");
                                                                                    DeclineMessageRequstButton.setVisibility(View.INVISIBLE);
                                                                                    DeclineMessageRequstButton.setEnabled(false);

                                                                            }
                                                                        });
                                                            }
                                                        }
                                                    });
                                           }
                                       }
                                   });
                       }
                    }
                });
    }

   private void CancelChatRequest()
    {
         ChatRequstRef.child(sendUserID).child(receiverUserId)
                 .removeValue()
                 .addOnCompleteListener(new OnCompleteListener<Void>()
                 {
                     @Override
                     public void onComplete(@NonNull Task<Void> task)
                     {
                        if (task.isSuccessful())
                        {
                            ChatRequstRef.child(receiverUserId).child(sendUserID)
                                    .removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>()
                                    {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if (task.isSuccessful())
                                            {

                                                sendmessagerequstButton.setEnabled(true);
                                                Current_state ="new";
                                                sendmessagerequstButton.setText("Send Message");
                                                DeclineMessageRequstButton.setVisibility(View.INVISIBLE);
                                                DeclineMessageRequstButton.setEnabled(false);
                                            }
                                        }
                                    });
                        }
                     }
                 });

    }

    private void SendChatRequst()
    {
        ChatRequstRef.child(sendUserID).child(receiverUserId)
                .child("request_type").setValue("sent")
                .addOnCompleteListener(new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        if (task.isSuccessful())
                        {
                            ChatRequstRef.child(receiverUserId).child(sendUserID)
                                    .child("request_type").setValue("received")
                                    .addOnCompleteListener(new OnCompleteListener<Void>()
                                    {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if (task.isSuccessful())
                                            {
                                                HashMap<String,String> chatNotificationMap=new HashMap<>();
                                                chatNotificationMap.put("from",sendUserID);
                                                chatNotificationMap.put("type","request");

                                                notificationRef.child(receiverUserId).push().setValue(chatNotificationMap)
                                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task)
                                                            {
                                                                if (task.isSuccessful())
                                                                {

                                                                    sendmessagerequstButton.setEnabled(true);
                                                                    Current_state="request_sent";
                                                                    sendmessagerequstButton.setText("Cancel Chat Request");

                                                                }



                                                            }
                                                        });
                                            }
                                        }
                                    });
                        }
                    }
                });

    }
}
