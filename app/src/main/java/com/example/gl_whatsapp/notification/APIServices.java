package com.example.gl_whatsapp.notification;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIServices
{
    @Headers(
            {"Content-Type:application/json",
                    "Authorization:key=AAAAPBIi-dM:APA91bHmY91eWB3LPAf0-gX43vSTvZ04kG5cOu1AWDvnXqi0hgXlu8le5KZ53xTw9KyzUcaMGBJ-RHTMzvDE_6IPGCx3gHH_Y58PTKxDMycJbTtRabmO9aY5B_EsZlk5-uiEYyJk6vAn"
    })
    @POST("fcm/send")
    Call<Response> sendNotification(@Body Sender body);

}
