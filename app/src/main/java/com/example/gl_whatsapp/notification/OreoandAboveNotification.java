package com.example.gl_whatsapp.notification;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.os.Build;

public class OreoandAboveNotification extends ContextWrapper
{
    private static final String ID="some_id";
    private static final String Name="Let's Chat";
    private NotificationManager notificationManager;

    public OreoandAboveNotification(Context base) {
        super(base);

        if (Build.VERSION.SDK_INT>=0)
        {
            createchannel();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createchannel()
    {
        NotificationChannel notificationChannel=new NotificationChannel(ID,Name,NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

        getManager().createNotificationChannel(notificationChannel);

    }
    public  NotificationManager getManager()
    {
        if (notificationManager==null)
        {
            notificationManager=(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public Notification.Builder getNotifications(String title, String body, PendingIntent pIntent, Uri soundUri, String icon)
    {
        return new Notification.Builder(getApplicationContext(),ID)
                .setContentIntent(pIntent)
                .setContentTitle(title)
                .setContentText(body)
                .setSound(soundUri)
                .setAutoCancel(true)
                .setSmallIcon(Integer.parseInt(icon));
    }
}
