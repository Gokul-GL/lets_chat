package com.example.gl_whatsapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;


public class AddFragment extends Fragment
{
    private View mView;
    private ImageView CameraVideo,GalleryVideo;
    private EditText DescriptionText;
    private ImageButton Upload;
    private static final int REQUEST_TAKE_GALLERY_VIDEO=1;
    private static final int REQUEST_TAKE_CAMERA_VIDEO=2;
    private StorageReference VideosRef;
    private FirebaseAuth mAuth;
    private String CurrentUserId;
    private DatabaseReference UsersRef,PostsRef;
    private Uri selectedImageUri;
    private String videofile,Description,State="play";
    private String saveCurrentDate, saveCurrentTime, postRandomName;
    private long countPosts=0;
    private ProgressDialog loadingBar;
    private VideoView vv;
    private FloatingActionButton fab1,fab2;
    private Boolean isFABOpen=false;

    public AddFragment()
    {



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        mView=inflater.inflate(R.layout.fragment_add, container, false);

        mAuth=FirebaseAuth.getInstance();
        CurrentUserId=mAuth.getCurrentUser().getUid();

        loadingBar=new ProgressDialog(getContext());

        VideosRef= FirebaseStorage.getInstance().getReference().child("Videos");
        PostsRef=FirebaseDatabase.getInstance().getReference().child("Posts");
        UsersRef= FirebaseDatabase.getInstance().getReference().child("Users");

//        CameraVideo=mView.findViewById(R.id.cameraVideo);
//        GalleryVideo=mView.findViewById(R.id.galleryVideo);
        DescriptionText=mView.findViewById(R.id.description);
        Upload=mView.findViewById(R.id.buttonPost);
        vv=mView.findViewById(R.id.videoView);
        FloatingActionButton fab =mView.findViewById(R.id.fab);
        fab1 =mView.findViewById(R.id.fab1);
        fab2 =mView.findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View view) {
                if(!isFABOpen)
                {

                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
               // Intent imintent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
               // startActivityForResult(imintent,REQUEST_TAKE_GALLERY_VIDEO);
                startActivityForResult(intent, REQUEST_TAKE_CAMERA_VIDEO);
            }
        });

        fab1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

               // Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
               // intent.setType("image/* video/*");
                Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Video"),REQUEST_TAKE_GALLERY_VIDEO);
            }
        });
//        CameraVideo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                // Intent imintent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                // startActivityForResult(imintent,REQUEST_TAKE_GALLERY_VIDEO);
//                startActivityForResult(intent, REQUEST_TAKE_CAMERA_VIDEO);
//            }
//        });
//
//        GalleryVideo.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//
//                // Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                // intent.setType("image/* video/*");
//                Intent intent = new Intent();
//                intent.setType("video/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent,"Select Video"),REQUEST_TAKE_GALLERY_VIDEO);
//            }
//        });


        Upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                loadingBar.setTitle("Uploading Post");
                loadingBar.setMessage("Please Wait");
                loadingBar.setCancelable(false);
                loadingBar.show();

                Description=DescriptionText.getText().toString();

                if (TextUtils.isEmpty(Description))
                {
                    Toast.makeText(getActivity(), " Please Add The Description ", Toast.LENGTH_SHORT).show();
                    loadingBar.dismiss();
                }
                else if (selectedImageUri==null)
                {
                    Toast.makeText(getActivity(), "Please Post video", Toast.LENGTH_SHORT).show();
                    loadingBar.dismiss();
                }
                else
                {
                    UploadVideos();
                }

            }
        });


        return mView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isFABOpen){
            closeFABMenu();
        }
    }

    @SuppressLint("RestrictedApi")
    private void showFABMenu(){
        isFABOpen=true;
        fab1.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fab2.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
        fab1.setVisibility(View.VISIBLE);
        fab2.setVisibility(View.VISIBLE);
    }

    @SuppressLint("RestrictedApi")
    private void closeFABMenu(){
        isFABOpen=false;
        fab1.animate().translationY(0);
        fab2.animate().translationY(0);
        fab1.setVisibility(View.INVISIBLE);
        fab2.setVisibility(View.INVISIBLE);
    }

    private void UploadVideos()
    {

        Calendar calFordDate = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMM  dd - yyyy");
        saveCurrentDate = currentDate.format(calFordDate.getTime());

        Calendar calFordTime = Calendar.getInstance();
        SimpleDateFormat currentTime = new SimpleDateFormat("hh:mm a");
        saveCurrentTime = currentTime.format(calFordTime.getTime());

        postRandomName= saveCurrentDate + saveCurrentTime;

        StorageReference filePath=VideosRef.child("Videos").child(postRandomName+".mp4");

        filePath.putFile(selectedImageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful())
                {

                    videofile=Objects.requireNonNull(task.getResult().getDownloadUrl()).toString();
                   // vv.setVideoURI(Uri.parse(videofile));
                    StoringPost();
                }
                else
                {
                    Toast.makeText(getActivity(), ""+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void StoringPost()
    {

        PostsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists())
                {
                    countPosts=dataSnapshot.getChildrenCount();
                }
                else
                {
                    countPosts=0;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        UsersRef.child(CurrentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists())
                {
                    String userFullName = dataSnapshot.child("name").getValue().toString();
                    String userProfileImage = dataSnapshot.child("profileimage").getValue().toString();


                    HashMap postsMap = new HashMap();
                    postsMap.put("uid", CurrentUserId);
                    postsMap.put("date", saveCurrentDate);
                    postsMap.put("time", saveCurrentTime);
                    postsMap.put("description", Description);
                    postsMap.put("postvideo", videofile);
                    postsMap.put("profileimage", userProfileImage);
                    postsMap.put("name", userFullName);
                    postsMap.put("counter", countPosts);
                    String PostKey=CurrentUserId + postRandomName;
                    PostsRef.child(PostKey).updateChildren(postsMap).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()) {
                                SendUserToHomeActivity();
                                Toast.makeText(getActivity(), "Posted Successfulyy ......", Toast.LENGTH_SHORT).show();
                                DescriptionText.setText(" ");
                                loadingBar.dismiss();
                            } else {
                                Toast.makeText(getActivity(), "Error Occured while updating your post." + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                loadingBar.dismiss();
                                DescriptionText.setText(" ");
                            }
                        }
                    });
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

   private void SendUserToHomeActivity()
   {
       TabsAccessorAdapter.statusFragment.setFragment(StatusFragment.postFragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == REQUEST_TAKE_GALLERY_VIDEO)
            {
                selectedImageUri = data.getData();
                //vv.setVideoURI(Uri.parse(String.valueOf(selectedImageUri)));
                vv.setVideoURI(selectedImageUri);
                vv.start();

                vv.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        if (State.equals("pause")) {
                            vv.start();
                            vv.requestFocus();
                            State = "play";
                        } else if (State.equals("play")) {
                            vv.pause();
                            vv.requestFocus();
                            State = "pause";
                        }
                        return true;
                    }
                });
            }
            if (requestCode==REQUEST_TAKE_CAMERA_VIDEO)
            {
                selectedImageUri = data.getData();
                //vv.setVideoURI(Uri.parse(String.valueOf(selectedImageUri)));
                vv.setVideoURI(selectedImageUri);
                vv.start();
                vv.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        if (State.equals("pause")) {
                            vv.start();
                            vv.requestFocus();
                            State = "play";
                        } else if (State.equals("play")) {
                            vv.pause();
                            vv.requestFocus();
                            State = "pause";
                        }
                        return true;
                    }
                });
            }

        }


    }

}
