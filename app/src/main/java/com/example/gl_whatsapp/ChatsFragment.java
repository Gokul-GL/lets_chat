package com.example.gl_whatsapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gl_whatsapp.notification.Token;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;



public class ChatsFragment extends Fragment

{
    private View privateChatView;
    private DatabaseReference ChatRef,userRef;
    private FirebaseAuth mAuth;
    private RecyclerView userMessageList;
    private String currentUserID;



    public ChatsFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        privateChatView=inflater.inflate(R.layout.fragment_chats, container, false);

        mAuth=FirebaseAuth.getInstance();
        currentUserID=mAuth.getCurrentUser().getUid();
        ChatRef= FirebaseDatabase.getInstance().getReference().child("Contacts").child(currentUserID);
        userRef=FirebaseDatabase.getInstance().getReference().child("Users");
        userMessageList=privateChatView.findViewById(R.id.private_message_list);
        userMessageList.setLayoutManager(new LinearLayoutManager(getContext()));

//        AdView adView = new AdView(MainActivity.mainActivity);
//        adView.setAdSize(AdSize.BANNER);
//        adView.setAdUnitId(getString(R.string.addmob_app_id));
        updateToken(FirebaseInstanceId.getInstance().getToken());

        return privateChatView;
    }


    @Override
    public void onStart()
    {
        super.onStart();
        FirebaseRecyclerOptions<Contacts > options=new FirebaseRecyclerOptions.Builder<Contacts>()
                .setQuery(ChatRef,Contacts.class)
                .build();


        FirebaseRecyclerAdapter<Contacts,ChatsViewHolder> adapter=new FirebaseRecyclerAdapter<Contacts, ChatsViewHolder>(options)
        {
            @Override
            protected void onBindViewHolder(@NonNull final ChatsViewHolder holder, int position, @NonNull Contacts model)
            {
                final String usersIDs=getRef(position).getKey();
                final String[] retImage = {"default_image"};
                userRef.child(usersIDs).addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        if (dataSnapshot.exists())
                        {
                            if (dataSnapshot.hasChild("profileimage"))
                            {
                                retImage[0] =dataSnapshot.child("profileimage").getValue().toString();
                                Picasso.get().load(retImage[0]).placeholder(R.drawable.profile_image).into(holder.profileimage);

                            }
                            final String retName=dataSnapshot.child("name").getValue().toString();
                            final String retStatus=dataSnapshot.child("status").getValue().toString();
                            holder.userName.setText(retName);



                            if (dataSnapshot.child("userState").hasChild("state"))
                            {
                                String state=dataSnapshot.child("userState").child("state").getValue().toString();
                                String date=dataSnapshot.child("userState").child("date").getValue().toString();
                                String time=dataSnapshot.child("userState").child("time").getValue().toString();

                                if (state.equals("online"))
                                {
                                    holder.userStatus.setText("online");

                                }
                               else if (state.equals("offline"))
                               {
                                   holder.userStatus.setText("Last Seen :" + date +" "+ time );
                               }


                            }
                            else
                            {
                                holder.userStatus.setText("offline");
                            }


                            holder.itemView.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    Intent chatintent=new Intent(getContext(),ChatActivity.class);
                                    chatintent.putExtra("visit_user_id",usersIDs);
                                    chatintent.putExtra("visit_user_name",retName);
                                    chatintent.putExtra("visit_image", retImage[0]);
                                    startActivity(chatintent );

                                }
                            });

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });


            }

            @NonNull
            @Override
            public ChatsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
            {
                View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_display_layout,viewGroup,false);
                return new ChatsViewHolder(view);
            }
        };
        userMessageList.setAdapter(adapter);
        adapter.startListening();
    }
    private void updateToken(String token)
    {
        DatabaseReference reference=FirebaseDatabase.getInstance().getReference("Tokens");
        Token token1=new Token(token);
        reference.child(currentUserID).setValue(token1);

    }
    public static class  ChatsViewHolder extends RecyclerView.ViewHolder
    {

        CircleImageView profileimage;
        TextView userStatus,userName;
        ImageButton callbutton;

        public ChatsViewHolder(@NonNull View itemView)
        {
            super(itemView);
            profileimage=itemView.findViewById(R.id.users_profile_image);
            userStatus=itemView.findViewById(R.id.user_status);
            userName=itemView.findViewById(R.id.user_profile_name);
            callbutton=itemView.findViewById(R.id.imageButton);
            callbutton.setVisibility(View.INVISIBLE);


        }
    }








}
