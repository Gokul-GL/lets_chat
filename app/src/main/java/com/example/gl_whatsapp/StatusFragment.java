package com.example.gl_whatsapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.app.VoiceInteractor;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.support.design.widget.BottomNavigationView.*;


public class StatusFragment extends Fragment
{


    private View myView;
     private Button button;
    private FirebaseAuth mAuth;
    private FirebaseUser CurrentUser;
    private BottomNavigationView mMainView;
    private FrameLayout MainFrame;
    public static PostsFragment postFragment=new PostsFragment();
    public static AddFragment  addFragment=new AddFragment();
    public StatusFragment()
       {


       }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        myView= inflater.inflate(R.layout.fragment_status, container, false);
        mAuth=FirebaseAuth.getInstance();
        CurrentUser=mAuth.getCurrentUser();
        setFragment(postFragment);
        final BottomNavigationView mMainView=myView.findViewById(R.id.main_nav);
        MainFrame=myView.findViewById(R.id.main_frame);
        mMainView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId())
                {

                    case R.id.nav_home:
                        mMainView.setItemBackgroundResource(R.color.colorWhite);
                        setFragment(postFragment);
                        return true;
                    case R.id.nav_add:
                        mMainView.setItemBackgroundResource(R.color.colorWhite);
                        setFragment(addFragment);
                        return true;

                    default:
                        return false;
                }
            }
        });

        return myView;


    }

    public static FragmentTransaction fragmentTransaction;

    public void setFragment(Fragment fragment)
    {

        fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction
                .replace(R.id.main_frame, fragment);
        fragmentTransaction.addToBackStack(null).commit();


//        FragmentTransaction fragmentTransaction=getChildFragmentManager().beginTransaction();
//        fragmentTransaction.replace(R.id.main_frame,fragment);
//        fragmentTransaction.commit();

    }

    }