package com.example.gl_whatsapp;

/**
 * Created by Linus on 05/08/18.
 */

public class Comments
{
    public String comment,date,time,name;

    public Comments()
    {

    }

    public Comments(String name,String comment, String date, String time)
    {
        this.name = name;
        this.comment = comment;
        this.date = date;
        this.time = time;

    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
