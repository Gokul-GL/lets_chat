package com.example.gl_whatsapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.LinearInterpolator;

import com.agrawalsuneet.dotsloader.loaders.LazyLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirstActivity extends AppCompatActivity
{


    private LazyLoader containerLL;
    private FirebaseUser currentUser;
    private FirebaseAuth mAthu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        containerLL=findViewById(R.id.ll);

        LazyLoader loader = new LazyLoader(this, 30, 20, ContextCompat.getColor(this, R.color.loader_selected),
                ContextCompat.getColor(this, R.color.loader_selected),
                ContextCompat.getColor(this, R.color.loader_selected));
        loader.setAnimDuration(500);
        loader.setFirstDelayDuration(100);
        loader.setSecondDelayDuration(200);
        loader.setInterpolator(new LinearInterpolator());

        containerLL.addView(loader);


        mAthu=FirebaseAuth.getInstance();
        currentUser=mAthu.getCurrentUser();

        if (currentUser==null)
        {
            // Splash();
            SendUserToMainActivity();
        }
        else{
            Splash();
        }


    }
    public void Splash()
    {
        if(!isConnected(FirstActivity.this))
        {
            buildDialog(FirstActivity.this).show();
        }
        else {
            Thread splash = new Thread() {
                @Override
                public void run() {


                    try {
                        sleep(3000);


                    } catch (Exception e) {

                    }

                    startActivity(new Intent(FirstActivity.this, MainActivity.class));
                    finish();


                }

            };
            splash.start();
        }

    }
    public void SendUserToMainActivity()
    {
        if(!isConnected(FirstActivity.this))
        {
            buildDialog(FirstActivity.this).show();
        }
        else {
            Thread splash = new Thread() {
                @Override
                public void run() {


                    try {
                        sleep(3000);


                    } catch (Exception e) {

                    }


                    startActivity(new Intent(FirstActivity.this, SplashActivity.class));
                    finish();


                }

            };
            splash.start();
        }
    }




    public boolean isConnected(Context context)
    {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting())
        {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;

        }
        else
        {
            return false;
        }

        return false;
    }

    public AlertDialog.Builder buildDialog(Context c)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press ok to Exit");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener()
        {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });

        return builder;
    }


}
