package com.example.gl_whatsapp;

public class Contacts
{
  public  String name,status,profileimage;


  public Contacts()
  {

  }

    public Contacts(String name, String status, String profileimage) {
        this.name = name;
        this.status = status;
        this.profileimage = profileimage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

}
