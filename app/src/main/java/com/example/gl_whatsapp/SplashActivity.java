package com.example.gl_whatsapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.felipecsl.gifimageview.library.GifImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashActivity extends AppCompatActivity
{//private int progressStatus = 0;

   // private ProgressBar progressBar;
    //private Handler handler = new Handler();
    private TextView animation_txt,txt;
    private ImageView im,im2;
    private FirebaseUser currentUser;
    private FirebaseAuth mAthu;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);




        animation_txt=findViewById(R.id.textView3);
        im=findViewById(R.id.splash);
        im2=findViewById(R.id.imageView2);
        txt=findViewById(R.id.textView2);


        mAthu=FirebaseAuth.getInstance();
        currentUser=mAthu.getCurrentUser();
        if (currentUser==null)
        {
            Thread splash = new Thread() {
                @Override
                public void run()
                {


                    try {
                        sleep(1700);


                    } catch (Exception e)
                    {

                    }

                    SendUserToLoginActivity();
//                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
//                    finish();


                }

            };
            splash.start();


        }
        else {
            Thread splash = new Thread() {
                @Override
                public void run() {


                    try {
                        sleep(1700);


                    } catch (Exception e) {

                    }


                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();


                }

            };
            splash.start();
        }




    }
    private void SendUserToLoginActivity()
    {
        Intent loginintent=new Intent(SplashActivity.this,LoginActivity.class);
        loginintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginintent);
        finish();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Animation animation=AnimationUtils.loadAnimation(this,R.anim.animation);
        animation_txt.startAnimation(animation);
        txt.startAnimation(animation);
        im.startAnimation(animation);
        im2.startAnimation(animation);
    }

//    private void startAnimation()
//    {
//        Animation animation=AnimationUtils.loadAnimation(this,R.anim.animation);
//        animation_txt.startAnimation(animation);
//
//    }





}
